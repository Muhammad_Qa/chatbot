<?php

use LDAP\Result;

class Database
{
    protected $servername;
    protected $username;
    protected $password;
    protected $dbname;
    protected $conn;
    public static $instance;
    public function __construct()
    {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->dbname = "chatbot_db";
        self::$instance = $this;
    }

    public static function get()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function connect()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }
    public  function create($attribute)
    {
        $this->connect();
        $sesion_id = 1;
        if (isset($attribute['sesion_id'])) {
            $sesion_id = $attribute['sesion_id'];
        }
        $site = 1;
        if (isset($attribute['site'])) {
            $site = $attribute['site'];
        }
        $answer = "";
        if (isset($attribute['answer'])) {
            $answer = $attribute['answer'];
        }
        $date = new DateTime();
        $newDate = $date->format('Y-m-d H:i:s');
        $sql = "INSERT INTO enquiries (`sesion_id`,`question`,`answer`, `created`, `site`)
VALUES ('" . $sesion_id . "', '" . $attribute['question'] . "','" . $answer . "','" . $newDate . "','" . $site . "')";
        $this->query($sql);
        $this->conn->close();
    }
    public function update($id, $attribute)
    {
        $this->connect();
        $sql =   "UPDATE enquiries SET answer = '" . $attribute["answer"] . "' WHERE ID = " . $id . ";";
        $this->query($sql);
    }
    public function delete($id)
    {
        $this->connect();
        $sql = "DELETE FROM enquiries WHERE id=" . $id . ";";
        $this->query($sql);
    }
    public function query($sql)
    {
        if ($this->conn->query($sql) === TRUE) {
            //    echo "New record created successfully";
        } else {
            //    echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    public function search()
    {
        $this->connect();
        $sql = "SELECT * FROM enquiries";
        $result = $this->conn->query($sql);
        return $result;
    }
}
