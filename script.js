
var sesion_id = makeid(5);
//create a new WebSocket object.
var msgBox = $('#message-box');
var wsUri = "ws://localhost:9000/example/Chat-Using-WebSocket-and-PHP-Socket/server.php";

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}








		//console.log();
		websocket = new WebSocket(wsUri);



		websocket.onopen = function(ev) { // connection is open 
			msgBox.append('<div class="system_msg" style="color:#bbbbbb">Welcome to my "Demo WebSocket Chat box"!</div>'); //notify user
		}
		// Message received from server
		websocket.onmessage = function(ev) {


      



			var response = JSON.parse(ev.data); //PHP sends Json data

			var res_type = response.type; //message type
			var user_message = response.message; //message text
      var question = response.question; //message text

			var user_name = response.name; //user name
			var user_color = response.color; //color
			var server_sesion_id = response.sesion_id; //color
			console.log(sesion_id)
			if (server_sesion_id == sesion_id) {
        hideLoading()
        
        console.log(response)
        var chatbotResponse = response.question;

        const botMessageDiv = document.createElement('div');
        botMessageDiv.classList.add('bot-message');
        botMessageDiv.textContent = 'Bot: ' + chatbotResponse;
  
        $("#chatMessages").append(botMessageDiv);
       // $("#chatMessages").scrollTop = $("#chatMessages").scrollHeight;
        $('#chatMessages').scrollTop($('#chatMessages')[0].scrollHeight);

        chatHistory.push({
          role: "assistant",
          content: "hello we will send answer when we are avilable "
        });

				// switch (res_type) {
				// 	case 'usermsg':
				// 		msgBox.append('<div><span class="user_name" style="color:' + user_color + '">' + user_name + '</span> : <span class="user_message">' + user_message + '</span></div>');
				// 		break;
				// 	case 'system':
				// 		msgBox.append('<div style="color:#bbbbbb">' + user_message + '</div>');
				// 		break;
				// }
				// msgBox[0].scrollTop = msgBox[0].scrollHeight; //scroll message 

			};
		};

		websocket.onerror = function(ev) {
			msgBox.append('<div class="system_error">Error Occurred - ' + ev.data + '</div>');
		};
		websocket.onclose = function(ev) {
			msgBox.append('<div class="system_msg">Connection Closed</div>');
		};

		//Message send button
		$('#sendMessagebuttom').click(function() {
			send_message();
		});

		//User hits enter key 
		$("#sendMessagebuttom").on("keydown", function(event) {
			if (event.which == 13) {
				send_message();
			}
		});











		//Send message
		function send_message() {

      var userInput = $('#userInput').val();
      const userMessage = userInput;
      if (userMessage.trim() === '') return;
      if (!isPhrase(userMessage))
        return;
        const chatbox = document.getElementById('chatbox');
        const userMessageDiv = document.createElement('div');
        userMessageDiv.classList.add('user-message');
        userMessageDiv.textContent = userMessage;
    
        chatMessages.appendChild(userMessageDiv);
        showLoading()
        $('#chatMessages').scrollTop($('#chatMessages')[0].scrollHeight);

			//prepare json data
			var msg = {
				question: userInput,
				name: userInput,
				color: "red",
				sesion_id: sesion_id
			};
			//convert and send data to server
			websocket.send(JSON.stringify(msg));
      $('#userInput').val("")
		}







function showDialog() {
    if ($(".chat-container").is(":visible"))
      $(".chat-container").hide("slide")
    else {
      $(".chat-container").show("slide")
    }
  }

  function sendMessage() {


   






















   
  

    // Send user message to PHP script using AJAX
    $.ajax({
      type: 'POST',
      url: 'submit.php',
      data: {
        userInput: userInput  
      },
      success: function(response) {
       

        // Append chatbot's response to the chatbox
      },error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
    }       
    });
  }























  
  //---------------------------------------
  const apiKey = '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';
  // --------------------------------------



  function isPhrase(str) {
    // Split the string into words using whitespace as the delimiter
    const words = str.split(/\s+/);

    // Check if there are more than one word
    return words.length > 1;
  }

  document.addEventListener("keypress", function(event) {
    // If the user presses the "Enter" key on the keyboard
    if (event.key === "Enter") {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      sendMessageButton.click();
    }
  });
  var sendMessageButton = document.getElementById('sendMessagebuttom');



  function showLoading() {
    var chatMessages = document.getElementById('chatMessages');
    var loadingDiv = document.createElement('div');
    loadingDiv.classList.add('loading');
    loadingDiv.textContent = 'Loading...';

    chatMessages.appendChild(loadingDiv);
  }

  function hideLoading() {
    const loadingDiv = document.querySelector('.loading');
    if (loadingDiv) {
      $("#chatMessages .loading").remove();
    }
  }

  const chatHistory = [{
    role: "system",
    content: "a simple bot to talke about web design"
  }];

  async function getBotResponseFromChatGPT(userMessage, chatHistory) {
    const apiUrl = 'https://api.openai.com/v1/chat/completions';

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${apiKey}`
    };

    chatHistory.push({
      role: "user",
      content: userMessage
    });

    const requestData = {
      model: "gpt-3.5-turbo",
      messages: chatHistory,
    };

    const response = await fetch(apiUrl, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(requestData)
    });

    const data = await response.json();
    const botResponse = data.choices[0].message.content;

    return botResponse;
  }
