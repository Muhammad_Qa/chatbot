<?php
$colors = array('#007AFF', '#FF7000', '#FF7000', '#15E25F', '#CFC700', '#CFC700', '#CF1100', '#CF00BE', '#F00');
$color_pick = array_rand($colors);
?>

<!DOCTYPE html>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		.chat-wrapper {
			font: bold 11px/normal 'lucida grande', tahoma, verdana, arial, sans-serif;
			background: #00a6bb;
			padding: 20px;
			margin: 20px auto;
			box-shadow: 2px 2px 2px 0px #00000017;
			max-width: 700px;
			min-width: 500px;
		}

		#message-box {
			width: 97%;
			display: inline-block;
			height: 300px;
			background: #fff;
			box-shadow: inset 0px 0px 2px #00000017;
			overflow: auto;
			padding: 10px;
		}

		.user-panel {
			margin-top: 10px;
		}

		input[type=text] {
			border: none;
			padding: 5px 5px;
			box-shadow: 2px 2px 2px #0000001c;
		}

		input[type=text]#name {
			width: 20%;
		}

		input[type=text]#message {
			width: 60%;
		}

		button#send-message {
			border: none;
			padding: 5px 15px;
			background: #11e0fb;
			box-shadow: 2px 2px 2px #0000001c;
		}
	</style>
</head>

<body>

	<div class="chat-wrapper">
		<div id="message-box"></div>
		<div class="user-panel">
			<input type="text" name="name" id="name" placeholder="Your Name" maxlength="15" />
			<input type="text" name="message" id="message" placeholder="Type your message here..." maxlength="100" />
			<button id="send-message">Send</button>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script language="javascript" type="text/javascript">






	</script>
</body>

</html>