<!DOCTYPE html>
<html>

<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <title>Chatbot</title>

  <link rel="stylesheet" href="style.css">

  <script src="script.js"></script>
</head>

<body>
  <div class="container mt-4">



    <div class="row">
      <div class="col-lg-6">
        <h1>Enquiries from all sites </h1>
      </div>
      <div class="col-lg-6">
        <button type="button" class="btn btn-outline-primary" data-bs-toggle='modal' href='#CreateModal' role='button'>add new enquiry </button>
      </div>
    </div>
    <?php
    // Step 1: Connect to the database
    $servername = "localhost"; // Replace with your server name
    $username = "root"; // Replace with your MySQL username
    $password = ""; // Replace with your MySQL password
    $dbname = "chatbot_db"; // Replace with your MySQL database name

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    // Step 2: Execute a query to fetch data
    $sql = "SELECT * FROM enquiries";
    $result = $conn->query($sql);

    // Step 3: Fetch and display data in an HTML table
    if ($result !== false && $result->num_rows > 0) {
      echo "<table  class='table'>
            <tr class='head-table'>
                <th  scope='col'>session id</th>
                <th  scope='col'>question</th>
                <th scope='col'>answer</th>
                <th scope='col'>created at </th>
                <th scope='col'>action </th>
            </tr>";

      while ($row = $result->fetch_assoc()) {
        $delete = "<button class='btn btn-danger' data-bs-toggle='modal' href='#exampleModalToggle' role='button'>delete</button>";
        if (!$row["answer"]) {
          $answer = "<button  class='btn btn-primary btn-answer' data-bs-toggle='modal' data-bs-target='#exampleModal' data-bs-whatever='@getbootstrap'data-action='" . $row["id"] . "'>answer </button>";
        } else {
          $answer = $row["answer"];
        }
        echo "<tr>
                <td>" . $row["sesion_id"] . "</td>
                <td>" . $row["question"] . "</td>
                <td>" . $answer . "</td>
                <td>" . $row["created"] . "</td>
                <td>" . $delete . "</td>

              </tr>";
      }
      echo "</table>";
    } else {
      echo "No records found";
    }

    // Step 4: Close the database connection
    $conn->close();
    ?>


    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">New answer</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form>
              <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Recipient:</label>
                <label for="recipient-name" class="col-form-label">muhammad</label>

              </div>
              <div class="mb-3">
                <label for="message-text" class="col-form-label">Message:</label>
                <textarea class="form-control" id="message-text"></textarea>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Send message</button>
          </div>
        </div>
      </div>
    </div>



    <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalToggleLabel">delete enquiry </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            are you sure you waqnt to delete this enquiry ?
          </div>
          <div class="modal-footer">
            <button class="btn btn-danger" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">delete </button>
            <button class="btn btn-secondary" data-bs-dismiss="modal">close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalToggleLabel2">deleted</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            you have delete enquiry.
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" data-bs-dismiss="modal">done</button>
          </div>
        </div>
      </div>
    </div>
  </div>
























































  <div class="modal fade" id="CreateModal" tabindex="-1" role="dialog" aria-labelledby="CreateModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="CreateModalsd">New enquiry</h5>
          <button type="button" class="close" data-dismiss="CreateModal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">question:</label>
              <input type="text" name="question" class="form-control question" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">answer:</label>
              <textarea class="form-control answer" id="message-text"></textarea>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="Create()">save enquiry</button>
        </div>
      </div>
    </div>
  </div>
</body>

</html>
<script>
</script>
<script>
  $(".btn-answer").on("click", "", function() {
    sdsd = 0;
    console.log($(this).attr("data-action"))
  });



  function requset(body) {
    console.log(body)
    $.ajax({
      type: 'POST',
      url: 'action.php',
      data: body,
      success: function(response) {
        return response;
        alert(" Error: " + errorThrown);
      }
    });
  }

  function Create() {
    $(".question").val()
    $(".answer").val();
    attrebute = {
      "action": "create",
      attrebute: {
        "question": $(".question").val(),
        "answer": $(".answer").val()
      }
    }
    requset(attrebute)

  }

  function Update() {

  }

  function Delete() {

  }

  function Search() {

  }
</script>
<script>
  var sesion_id = makeid(5);
  //create a new WebSocket object.
  var msgBox = $('#message-box');
  var wsUri = "ws://localhost:9000/example/Chat-Using-WebSocket-and-PHP-Socket/server.php";

  function makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
  }








  //console.log();
  websocket = new WebSocket(wsUri);



  websocket.onopen = function(ev) { // connection is open 
    msgBox.append('<div class="system_msg" style="color:#bbbbbb">Welcome to my "Demo WebSocket Chat box"!</div>'); //notify user
  }
  // Message received from server
  websocket.onmessage = function(ev) {






    var response = JSON.parse(ev.data); //PHP sends Json data

    var res_type = response.type; //message type
    var user_message = response.message; //message text
    var question = response.question; //message text

    var user_name = response.name; //user name
    var user_color = response.color; //color
    var created = response.created; //color
    var server_sesion_id = response.sesion_id; //color
    console.log(sesion_id)
    hideLoading()

    console.log(response)
    var chatbotResponse = response.question;

    if (server_sesion_id) {

      $(`<tr class="bg-success">
        <td> ` + server_sesion_id + `</td>
                <td>` + question + `</td>
                <td><button  class='btn btn-primary' data-bs-toggle='modal' data-bs-target='#exampleModal' data-bs-whatever='@getbootstrap'>answer </button></td>
                <td>` + created + `</td>
                <td><button class='btn btn-danger' data-bs-toggle='modal' href='#exampleModalToggle' role='button'>delete</button></td>
        </tr>`).insertAfter(".head-table")

      const botMessageDiv = document.createElement('div');
      botMessageDiv.classList.add('bot-message');
      botMessageDiv.textContent = 'Bot: ' + chatbotResponse;

      $("#chatMessages").append(botMessageDiv);
      // $("#chatMessages").scrollTop = $("#chatMessages").scrollHeight;
      $('#chatMessages').scrollTop($('#chatMessages')[0].scrollHeight);

      chatHistory.push({
        role: "assistant",
        content: "hello we will send answer when we are avilable "
      });

      // switch (res_type) {
      // 	case 'usermsg':
      // 		msgBox.append('<div><span class="user_name" style="color:' + user_color + '">' + user_name + '</span> : <span class="user_message">' + user_message + '</span></div>');
      // 		break;
      // 	case 'system':
      // 		msgBox.append('<div style="color:#bbbbbb">' + user_message + '</div>');
      // 		break;
      // }
      // msgBox[0].scrollTop = msgBox[0].scrollHeight; //scroll message 

    };;
  }
  websocket.onerror = function(ev) {
    msgBox.append('<div class="system_error">Error Occurred - ' + ev.data + '</div>');
  };
  websocket.onclose = function(ev) {
    msgBox.append('<div class="system_msg">Connection Closed</div>');
  };

  //Message send button
  $('#sendMessagebuttom').click(function() {
    send_message();
  });

  //User hits enter key 
  $("#sendMessagebuttom").on("keydown", function(event) {
    if (event.which == 13) {
      send_message();
    }
  });











  //Send message
  function send_message() {

    var userInput = $('#userInput').val();
    const userMessage = userInput;
    if (userMessage.trim() === '') return;
    if (!isPhrase(userMessage))
      return;
    const chatbox = document.getElementById('chatbox');
    const userMessageDiv = document.createElement('div');
    userMessageDiv.classList.add('user-message');
    userMessageDiv.textContent = userMessage;

    chatMessages.appendChild(userMessageDiv);
    showLoading()
    $('#chatMessages').scrollTop($('#chatMessages')[0].scrollHeight);

    //prepare json data
    var msg = {
      question: userInput,
      name: userInput,
      color: "red",
      sesion_id: sesion_id
    };
    //convert and send data to server
    websocket.send(JSON.stringify(msg));
    $('#userInput').val("")
  }
</script>