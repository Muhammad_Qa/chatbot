<!DOCTYPE html>
<html>

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <title>Chatbot</title>

  <link rel="stylesheet" href="style.css">

  <script src="script.js"></script>
</head>

<body>
  <div class="chat-container">
    <div class="chat-header">
      Chat Bot
    </div>
    <div class="chat-messages" id="chatMessages">
      <!-- Chat messages will appear here -->
    </div>
    <div class="input-container">
      <input type="text" class="input-box" id="userInput" placeholder="Type your Message...">
      <button class="send-button" id="sendMessagebuttom" onclick="send_message()">Send</button>
    </div>
  </div>
  <button class="chat-button" onclick="showDialog()">
    &#9993;
  </button>
</body>

</html>
<script>
</script>
<script>
</script>